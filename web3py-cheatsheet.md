## Web3PY cheat sheet
Quick list of helpful commands in web3py. When using web3py I find it helpful to be able to quickly execute commands for testing purposes. By using pyenv with a virtualenv and ipython we get a really nice python shell with things like object autocomplete to work within. 


## Requirements 
If you want the deluxe setup you can do something like this: 


-[pyenv](https://github.com/pyenv/pyenv) with [pyenv virtualenv](https://github.com/pyenv/pyenv-virtualenv) 

-[ipython](https://ipython.org/) 

-[web3py](https://github.com/ethereum/web3.py) 

## Setup 
Assuming you already have pyenv with pyenv virtualenv setup, just run:

```bash 
pyenv install 3.8.5
pyenv virtualenv 3.8.5 Web3PY-3.8.5
```

Then either activate your pyenv virtualenv by directory with:

```bash 
pyenv local 3.8.5/envs/Web3PY-3.8.5
```

Or activate it manually for your current shell with:

```bash 
pyenv activate Web3PY-3.8.5
```

Install Web3:

```bash 
pip install web3py
```

Then activate your ipython shell:
```bash
ipython
```

![ipython web3py getting started](images/web3py-1.png)

If you're new to ipython try using tab complete on an Object. Also, make sure to try pressing enter on a Object function (without any params) so you can quickly see what params are expected and their types! 

![ipython web3py autocomplete](images/web3py-2.png)


## Web3PY Commands 
Some of these examples can be found in the [Web3PY docs](https://web3py.readthedocs.io/en/stable/index.html) 


### [Connecting Web3 to a provider](https://web3py.readthedocs.io/en/stable/providers.html#providers)

Create web3 provider object from a local node

```python
In [0]: w3_local = Web3(Web3.HTTPProvider('http://127.0.0.1:8545'))

In [1]: w3_local.isConnected()
Out[1]: True
```

Create web3 provider object from infura node

```python 
In [0]: w3_mainnet = Web3(Web3.HTTPProvider('https://mainnet.infura.io/v3/<infura-project-id>'))
```


### [Keccak hashing](https://web3py.readthedocs.io/en/stable/web3.main.html?#web3.Web3.solidityKeccak)

```ipython 
In [6]: Web3.solidityKeccak(['string'], ['MINTER_ROLE'])
Out [6]: HexBytes('0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6')
```



 
